# Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.

# Note: The algorithm should run in linear time and in O(1) space.

# Example 1:

# Input: [3,2,3]
# Output: [3]

# Example 2:

# Input: [1,1,1,3,3,2,2,2]
# Output: [1,2]

def majority_numbers(nums)
  majority = nums.length/3
  result = nums.uniq
  result.select { |num| nums.count(num) > majority }
  result
end

p majority_numbers([3, 2, 3]) == [3]
p majority_numbers([1, 1, 1, 3, 3, 2, 2, 2]) == [1, 2]
