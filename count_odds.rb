# Given two non-negative integers low and high. Return the count of odd numbers between low and high (inclusive).



# Example 1:

# Input: low = 3, high = 7
# Output: 3
# Explanation: The odd numbers between 3 and 7 are [3,5,7].

# Example 2:

# Input: low = 8, high = 10
# Output: 1
# Explanation: The odd numbers between 8 and 10 are [9].



# Constraints:

#     0 <= low <= high <= 10^9

def count_odds(low, high)
  low.even? && high.even? ? (high - low) / 2 : (high - low) / 2 + 1
end

p count_odds(3, 7) == 3
p count_odds(8, 10) == 1
p count_odds(5, 24) == 10
