# Count the number of prime numbers less than a non-negative number, n.

# Example 1:

# Input: n = 10
# Output: 4
# Explanation: There are 4 prime numbers less than 10, they are 2, 3, 5, 7.

# Example 2:

# Input: n = 0
# Output: 0

# Example 3:

# Input: n = 1
# Output: 0

# Constraints:

#     0 <= n <= 5 * 106
#
# Use Sieve of Erastosthenes
# How the sieve works:
# - Assume all numbers are prime and create a list
# - Starting with 2 take every number p and mark p^2, p^2 + p, p^p + 2p as not prime
# - Stop when the square of a number is greater than or equal to n

class Solution:
    def countPrimes(self, n: int) -> int:
        prime = [True for i in range(n)]
        p = 2
        count = 0
        while (p * p <= n):
            if (prime[p] == True):
                for i in range(p * p, n, p):
                    prime[i] = False
            p += 1

        for p in range(2, n):
            if prime[p]:
                count += 1

        return count
