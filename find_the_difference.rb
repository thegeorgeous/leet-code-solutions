def find_the_difference(s, t)
  new_s = s.split('')
  new_t = t.split('')
  new_s.each do |c|
    new_t.delete_at(new_t.index(c))
  end
  new_t.first
end

s = 'abcd'
t = 'abcde'

p find_the_difference(s, t) == 'e'
