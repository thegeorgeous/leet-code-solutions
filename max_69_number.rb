def maximum69_number (num)
  output = []
  digits = num.digits.reverse
  digits.each.with_index do |digit, index|
    if digit == 6
      digits[index] = 9
      output << digits.join.to_i
      digits[index] = 6
    end
  end
  output.max
end

p maximum69_number(9669) == 9969
p maximum69_number(669) == 969
