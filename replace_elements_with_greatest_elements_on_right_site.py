# Given an array arr, replace every element in that array with the greatest element among the elements to its right, and replace the last element with -1.

# After doing so, return the array.

# Example 1:

# Input: arr = [17,18,5,4,6,1]
# Output: [18,6,6,6,1,-1]

# Constraints:

#     1 <= arr.length <= 10^4
#     1 <= arr[i] <= 10^5

class Solution:
    def replaceElements(self, arr: List[int]) -> List[int]:
        max_so_far = arr[0]

        for i in range(len(arr)-1):
            if arr[i] == max_so_far:
                max_right = arr[i+1]
                for j in range(i+1, len(arr)):
                    if arr[j] > max_right:
                        max_right = arr[j]
                max_so_far = max_right

            arr[i] = max_so_far

        arr[-1] = -1
        return arr
