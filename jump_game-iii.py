# Given an array of non-negative integers arr, you are initially positioned at start index of the array. When you are at index i, you can jump to i + arr[i] or i - arr[i], check if you can reach to any index with value 0.

# Notice that you can not jump outside of the array at any time.

# Example 1:

# Input: arr = [4,2,3,0,3,1,2], start = 5
# Output: true
# Explanation:
# All possible ways to reach at index 3 with value 0 are:
# index 5 -> index 4 -> index 1 -> index 3
# index 5 -> index 6 -> index 4 -> index 1 -> index 3

# Example 2:

# Input: arr = [4,2,3,0,3,1,2], start = 0
# Output: true
# Explanation:
# One possible way to reach at index 3 with value 0 is:
# index 0 -> index 4 -> index 1 -> index 3

# Example 3:

# Input: arr = [3,0,2,1,2], start = 2
# Output: false
# Explanation: There is no way to reach at index 1 with value 0.

# Constraints:

#     1 <= arr.length <= 5 * 10^4
#     0 <= arr[i] < arr.length
#     0 <= start < arr.length


# This was the accepted solution in Daily Challenge
class Solution:
    def canReach(self, arr: List[int], start: int) -> bool:
        result = []
        queue = [start]
        while queue:
            current_level = []
            for _ in range(len(queue)):
                current_node = queue.pop(0)
                if arr[current_node] == 0: return True
                current_level.append(current_node)  # add node to current level
                if current_node - arr[current_node] >= 0:
                    queue.append(current_node - arr[current_node])
                if current_node + arr[current_node] < len(arr):
                    queue.append(current_node + arr[current_node])

            result += current_level
            # break loop if the entire array has been covered
            # Else it will loop endlessly
            if len(result) == len(arr): break
        return False

# This is the leetcode BFS solution
# Admittedly succint, but has almost identical runtime and memory usage
# and modifies the input array
# class Solution:
#     def canReach(self, arr: List[int], start: int) -> bool:
#         length = len(arr)
#         queue = [start]

#         while queue:
#             current_node = queue.pop(0)
#             if arr[current_node] == 0: return True
#             if arr[current_node] < 0: continue

#             for i in [current_node + arr[current_node], current_node - arr[current_node]]:
#                 if 0 <= i < length:
#                     queue.append(i)

#             arr[current_node] = -arr[current_node]

#         return False
#

# This is the leetcode DFS solution
# Same runtime and memory as the BFS solution
# class Solution:
#     def canReach(self, arr: List[int], start: int) -> bool:
#         if 0 <= start < len(arr) and arr[start] >= 0:
#             if arr[start] == 0:
#                 return True

#             arr[start] = -arr[start]
#             return self.canReach(arr, start+arr[start]) or self.canReach(arr, start-arr[start])

#         return False
