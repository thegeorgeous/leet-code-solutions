# A self-dividing number is a number that is divisible by every digit it contains.

# For example, 128 is a self-dividing number because 128 % 1 == 0, 128 % 2 == 0, and 128 % 8 == 0.

# Also, a self-dividing number is not allowed to contain the digit zero.

# Given a lower and upper number bound, output a list of every possible self dividing number, including the bounds if possible.

# Example 1:

# Input:
# left = 1, right = 22
# Output: [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 15, 22]

# Note:
# - The boundaries of each input argument are 1 <= left <= right <= 10000.

def self_dividing_numbers(left, right)
  output = []
  (left..right).each do |i|
    is_self_dividing = true
    digits = i.digits
    if digits.include? 0
      is_self_dividing = false
      next
    end
    if digits.count < 2
      output << i
      next
    end
    digits.each do |digit|
      rem = i % digit
      is_self_dividing &&= rem == 0
    end
    output << i if is_self_dividing
  end
  output
end
