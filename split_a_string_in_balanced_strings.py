# Balanced strings are those who have equal quantity of 'L' and 'R' characters.

# Given a balanced string s split it in the maximum amount of balanced strings.

# Return the maximum amount of splitted balanced strings.



# Example 1:

# Input: s = "RLRRLLRLRL"
# Output: 4
# Explanation: s can be split into "RL", "RRLL", "RL", "RL", each substring contains same number of 'L' and 'R'.

# Example 2:

# Input: s = "RLLLLRRRLR"
# Output: 3
# Explanation: s can be split into "RL", "LLLRRR", "LR", each substring contains same number of 'L' and 'R'.

# Example 3:

# Input: s = "LLLLRRRR"
# Output: 1
# Explanation: s can be split into "LLLLRRRR".

# Example 4:

# Input: s = "RLRRRLLRLL"
# Output: 2
# Explanation: s can be split into "RL", "RRRLLRLL", since each substring contains an equal number of 'L' and 'R'



# Constraints:

#     1 <= s.length <= 1000
#     s[i] = 'L' or 'R'
# Runtime: 20 ms, faster than 98.81% of Python3 online submissions for Split a String in Balanced Strings.
# Memory Usage: 14.2 MB, less than 99.97% of Python3 online submissions for Split a String in Balanced Strings.

class Solution:
    def balancedStringSplit(self, s: str) -> int:
        output = []
        l_count = 0
        r_count = 0
        stack = []

        for i in range(len(s)):
            stack.append(s[i])
            if s[i] == "L":
                l_count += 1
            else:
                r_count += 1

            if l_count == r_count:
                output.append("".join(stack))
                stack = []
                l_count = 0
                r_count = 0

        return len(output)
