# Given an array arr, replace every element in that array with the greatest element among the elements to its right, and replace the last element with -1.

# After doing so, return the array.

# Example 1:

# Input: arr = [17,18,5,4,6,1]
# Output: [18,6,6,6,1,-1]

# Constraints:

#     1 <= arr.length <= 10^4
#     1 <= arr[i] <= 10^5

def replace_elements(arr)
  output = []
  arr.length.times do |i|
    output.unshift(arr.slice(-i, i).max)
  end
  output[-1] = -1
  output
end
