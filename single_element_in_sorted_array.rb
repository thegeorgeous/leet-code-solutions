def single_non_duplicate(num)
 counter = {}
  nums.each do |num|
    counter[num] =  counter[num] ? counter[num] + 1 : 1
  end

  counter.select { |key, value| key if value == 1 }

end
