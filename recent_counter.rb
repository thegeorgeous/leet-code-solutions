class RecentCounter
  def initialize()
    @pings = []
  end

  def ping(t)
    @pings << t
    until @pings.empty?
      if @pings.first < t - 3000
        @pings.shift
      else
        break
      end
    end
    return @pings.length
  end
end

output = []
recent_counter = RecentCounter.new
output << recent_counter.ping(1)
output << recent_counter.ping(100)
output << recent_counter.ping(3001)
output << recent_counter.ping(3002)
p output
