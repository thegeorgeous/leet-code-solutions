def single_number(nums)
  counter = {}
  nums.each do |num|
    counter[num] =  counter[num] ? counter[num] + 1 : 1
  end

  counter.each do |key, value|
    nums.delete(key) if value > 1
  end

  nums
end

p single_number([1,2,1,3,2,5]) == [3, 5]
