# The Hamming distance between two integers is the number of positions at which the corresponding bits are different.

# Given two integers x and y, calculate the Hamming distance.

# Note:
# 0 ≤ x, y < 231.

# Example:

# Input: x = 1, y = 4

# Output: 2

# Explanation:
# 1   (0 0 0 1)
# 4   (0 1 0 0)
#        ↑   ↑

# The above arrows point to positions where the corresponding bits are different.

class Solution:
    def hammingDistance(self, x: int, y: int) -> int:
        # The high number of leading zeroes is to allow leetcode tests to pass
        string1 = '{:050b}'.format(x)
        string2 = '{:050b}'.format(y)

        print(string1, string2)
        dist_counter = 0
        for n in range(len(string1)):
            if string1[n] != string2[n]:
                dist_counter += 1
        return dist_counter
