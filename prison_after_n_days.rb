def prison_after_n_days(cells, n)
  result_set = []
  i = 0
  while i < 14
    output = cells.dup
    (0..7).each do |j|
      if j == 0 or j == 7
        output[j] = 0
      elsif (cells[j-1] == 0 && cells[j+1] == 0) || (cells[j-1] == 1 && cells[j+1] == 1)
        output[j] = 1
      else
        output[j] = 0
      end
    end
    result_set << output
    cells = output.dup
    i += 1
  end

  result_index = n % 14
  return result_set[result_index-1]
end

cells = [0,1,0,1,1,0,0,1]
N = 7

p prison_after_n_days(cells, N) ==[0,0,1,1,0,0,0,0]
