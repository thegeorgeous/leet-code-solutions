def merge(intervals)
  output = []
  intervals = intervals.sort

  intervals.each do |interval|
    if output.empty? || output.last[1] < interval[0]
      output << interval
    else
      output.last[1] = [output.last[1], interval[1]].max
    end
  end

  output
end

p merge([[1, 3], [2, 6], [8, 10], [15, 18]]) == [[1, 6], [8, 10], [15, 18]]
p merge([[1, 4], [4, 5]]) == [[1, 5]]
p merge([[1, 5]]) == [[1, 5]]
p merge([[1, 4], [0, 4]]) == [[0, 4]]
p merge([[1, 4], [2, 3]]) == [[1, 4]]
