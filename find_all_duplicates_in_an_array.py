# Given an array of integers, 1 ≤ a[i] ≤ n (n = size of array), some elements appear twice and others appear once.

# Find all the elements that appear twice in this array.

# Could you do it without extra space and in O(n) runtime?

# Example:

# Input:
# [4,3,2,7,8,2,3,1]

# Output:
# [2,3]
# Note: Can be solved using a hash and filtering. This solves it O(1) time and
# O(n) space This uses the array like a hash and uses negation as a key. It's
# possible to use it as a hash because the numbers are always smaller than the
# size of the array.

class Solution:
    def findDuplicates(self, nums: List[int]) -> List[int]:
        output = []

        for num in nums:
            if nums[abs(num)-1] < 0:
                output.append(abs(num))
            else:
                nums[abs(num)-1] *= -1

        return output
