# Given a string s, find the length of the longest substring without repeating characters.

# Example 1:

# Input: s = "abcabcbb"
# Output: 3
# Explanation: The answer is "abc", with the length of 3.

# Example 2:

# Input: s = "bbbbb"
# Output: 1
# Explanation: The answer is "b", with the length of 1.

# Example 3:

# Input: s = "pwwkew"
# Output: 3
# Explanation: The answer is "wke", with the length of 3.
# Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

# Example 4:

# Input: s = ""
# Output: 0

# Constraints:

#     0 <= s.length <= 5 * 104
#     s consists of English letters, digits, symbols and spaces.
# TODO: Solution is brute force and not optimized. Add optimized solution

# class Solution:
#     def lengthOfLongestSubstring(self, s: str) -> int:
#         max_length = 0
#         chars = []
#         for i in range(len(s)):
#             chars = [s[i]]
#             j = i+1
#             while j < len(s):
#                 if s[j] in chars:
#                     break
#                 else:
#                     chars.append(s[j])
#                 if len(chars) > max_length:
#                     max_length = len(chars)

#                 j += 1
#         if max_length == 0: max_length = len(chars)
#         return max_length

# Optimized solution

class Solution:
    def lengthOfLongestSubstring(self, s: str) -> int:
        n = len(s)
        ans = 0

        mp = {}

        i = 0

        for j in range(n):
            print(j)
            print("i = ", i)
            print(mp)
            print("ans = ", ans)
            print("#########")
            if s[j] in mp:
                i = max(mp[s[j]], i)

            ans = max(ans, j - i + 1)
            mp[s[j]] = j + 1

        return ans

print(Solution().lengthOfLongestSubstring("abcabcbb") == 3)
