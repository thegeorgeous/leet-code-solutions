# Given an array of size n, find the majority element. The majority element is the element that appears more than ⌊ n/2 ⌋ times.

# You may assume that the array is non-empty and the majority element always exist in the array.

# Example 1:

# Input: [3,2,3]
# Output: 3

# Example 2:

# Input: [2,2,1,1,1,2,2]
# Output: 2

def majority_element(nums)
  majority = nums.length/2
  result = nums.uniq
  result = result.select { |num| nums.count(num) > majority }
  result.first
end

p majority_element([3,2,3]) == 3
p majority_element([2,2,1,1,1,2,2]) == 2
