# Given a non-empty binary tree, return the average value of the nodes on each level in the form of an array.

# Example 1:

# Input:
#     3
#    / \
#   9  20
#     /  \
#    15   7
# Output: [3, 14.5, 11]
# Explanation:
# The average value of nodes on level 0 is 3,  on level 1 is 14.5, and on level 2 is 11. Hence return [3, 14.5, 11].

# Note:

#     The range of node's value is in the range of 32-bit signed integer.

# Definition for a binary tree node.
# class TreeNode:
#     def __init__(self, val=0, left=None, right=None):
#         self.val = val
#         self.left = left
#         self.right = right
class Solution:
    def averageOfLevels(self, root: TreeNode) -> List[float]:
        result = []
        if not root: return result
        
        queue = [root]
        
        while queue:
            current_level = []
            for _ in range(len(queue)):
                current_node = queue.pop(0)
                current_level.append(current_node.val)
                
                if current_node.left:
                    queue.append(current_node.left)
                if current_node.right:
                    queue.append(current_node.right)
            result.append(sum(current_level)/len(current_level))
        
        return result
