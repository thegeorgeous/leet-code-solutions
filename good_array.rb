# Given an array nums of positive integers. Your task is to select some subset of nums, multiply each element by an integer and add all these numbers. The array is said to be good if you can obtain a sum of 1 from the array by any possible subset and multiplicand.

# Return True if the array is good otherwise return False.

# Example 1:

# Input: nums = [12,5,7,23]
# Output: true
# Explanation: Pick numbers 5 and 7.
# 5*3 + 7*(-2) = 1

# Example 2:

# Input: nums = [29,6,10]
# Output: true
# Explanation: Pick numbers 29, 6 and 10.
# 29*1 + 6*(-3) + 10*(-1) = 1

# Example 3:

# Input: nums = [3,6]
# Output: false

# Constraints:

#     1 <= nums.length <= 10^5
#     1 <= nums[i] <= 10^9
# Uses Bezout's formula

def is_good_array(nums)
  start = nums[0]
  return true if nums.length == 1 && nums.first == 1

  (1..nums.length-1).each do |i|
    d = gcd(start, nums[i])
    return true if d == 1

    start = d
  end
  return false
end

def gcd(a, b)
  while b != 0
    a, b = b, a % b
  end

  a
end

p is_good_array([12,5,7,23]) == true
p is_good_array([29, 6, 10]) == true
p is_good_array([3, 6]) == false
p is_good_array([6,10,15]) == true
p is_good_array([1]) == true
