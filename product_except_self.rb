# Given an array nums of n integers where n > 1,  return an array output such that output[i] is equal to the product of all the elements of nums except nums[i].

# Example:

# Input:  [1,2,3,4]
# Output: [24,12,8,6]

# Constraint: It's guaranteed that the product of the elements of any prefix or suffix of the array (including the whole array) fits in a 32 bit integer.

# Note: Please solve it without division and in O(n).

# Follow up:
# Could you solve it with constant space complexity? (The output array does not count as extra space for the purpose of space complexity analysis.)

def product_except_self(nums)
  output = []
  (0..nums.length - 1).each do |i|
    output << nums.select.with_index { |_, index| index != i }.reduce(:*)
  end

  output
end

p product_except_self([2,3,3,3]) == [27, 18, 18, 18]
p product_except_self([0, 0]) == [0, 0]
p product_except_self([1, 0]) == [0, 1]
