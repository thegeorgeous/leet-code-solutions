def shift_grid(grid, k)
  output = []
  m = grid.first.length
  n = grid.length
  grid = grid.flatten
  grid = grid.rotate(-k)
  start = 0

  while output.length < n
    output << grid.slice(start, m)
    start += m
  end
  output
end

p shift_grid([[1,2,3],[4,5,6],[7,8,9]], 1) == [[9,1,2],[3,4,5],[6,7,8]]
p shift_grid([[3,8,1,9],[19,7,2,5],[4,6,11,10],[12,0,21,13]], 4) == [[12,0,21,13],[3,8,1,9],[19,7,2,5],[4,6,11,10]]
