# Given the coordinates of four points in 2D space, return whether the four points could construct a square.

# The coordinate (x,y) of a point is represented by an integer array with two integers.

# Example:

# Input: p1 = [0,0], p2 = [1,1], p3 = [1,0], p4 = [0,1]
# Output: True

# Note:

#     All the input integers are in the range [-10000, 10000].
#     A valid square has four equal sides with positive length and four equal angles (90-degree angles).
#     Input points have no order.

class Solution:
    def validSquare(self, p1: List[int], p2: List[int], p3: List[int], p4: List[int]) -> bool:
        points = { tuple(p1), tuple(p2), tuple(p3), tuple(p4) }
        if len(points) < 4 : return False

        sides = set()
        sides.add(self.distance(p1, p2))
        sides.add(self.distance(p2, p3))
        sides.add(self.distance(p3, p4))
        sides.add(self.distance(p4, p1))
        sides.add(self.distance(p2, p4))
        sides.add(self.distance(p1, p3))

        if len(sides) == 2:
            return True

        return False


    def distance(self, p1, p2):
        x = p1[0] - p2[0]
        y = p1[1] - p2[1]

        print(x, y)
        print(sqrt(x**2 + y**2))
        return sqrt(x**2 + y**2)
