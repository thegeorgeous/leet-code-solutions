# Your task is to calculate ab mod 1337 where a is a positive integer and b is an extremely large positive integer given in the form of an array.



# Example 1:

# Input: a = 2, b = [3]
# Output: 8

# Example 2:

# Input: a = 2, b = [1,0]
# Output: 1024

# Example 3:

# Input: a = 1, b = [4,3,3,8,5,2]
# Output: 1

# Example 4:

# Input: a = 2147483647, b = [2,0,0]
# Output: 1198



# Constraints:

#     1 <= a <= 231 - 1
#     1 <= b.length <= 2000
#     0 <= b[i] <= 9
#     b doesn't contain leading zeros.


class Solution:
    def superPow(self, b: int, e: List[int]) -> int:
        m = 1337
        s = [str(i) for i in e]
        e = int("".join(s))
        r = 1
        b = b % m
        while e > 0:
            if e % 2 == 1:
                r = (r*b) % m
            e = e >> 1
            b = (b ** 2) % m
        return r
