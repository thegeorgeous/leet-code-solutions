# You are given an integer array nums sorted in ascending order, and an integer target.

# Suppose that nums is rotated at some pivot unknown to you beforehand (i.e., [0,1,2,4,5,6,7] might become [4,5,6,7,0,1,2]).

# If target is found in the array return its index, otherwise, return -1.



# Example 1:

# Input: nums = [4,5,6,7,0,1,2], target = 0
# Output: 4

# Example 2:

# Input: nums = [4,5,6,7,0,1,2], target = 3
# Output: -1

# Example 3:

# Input: nums = [1], target = 0
# Output: -1



# Constraints:

#     1 <= nums.length <= 5000
#     -10^4 <= nums[i] <= 10^4
#     All values of nums are unique.
#     nums is guranteed to be rotated at some pivot.
#     -10^4 <= target <= 10^4

# Explanation:
# - Find the smallest number in array. Use it to create the first bisection
# - Choose required half based on target on then do conventional binary search


class Solution:
    def search(self, nums: List[int], target: int) -> int:
        length = len(nums)
        left = 0
        right = length - 1

        while left < right:
            pivot = (left+right) // 2

            if nums[pivot] > nums[right]:
                left = pivot + 1
            else:
                right = pivot

        lowest = left
        left, right = 0, length - 1

        if nums[lowest] <= target and nums[right] >= target:
            left = lowest
        else:
            right = lowest

        while left <= right:
            pivot = (left+right) // 2

            if nums[pivot] == target:
                return pivot
            elif nums[pivot] > target:
                right = pivot - 1
            else:
                left = pivot + 1

        return -1
